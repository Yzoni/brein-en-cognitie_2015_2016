function [ v ] = bounds( up, low )
% returns a vector x with random numbers between [low,up].
if ~exist('low','var') || isempty(low)
  low=0;
end

v = (up - low) * rand() + low;

end

